<button {{ $attributes->merge(['class' => 'inline-flex items-center px-4 py-2 bg-red-500 border border-red-300 rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-red-300 active:bg-red-400 focus:outline-none focus:border-red-300 focus:ring focus:ring-red-200 disabled:opacity-25 transition']) }}>
    {{ $slot }}
</button>

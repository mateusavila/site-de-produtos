<x-app-layout>

    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('Adicionar Produto') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="py-4 px-4 bg-white dark:bg-gray-800 overflow-hidden shadow-sm sm:rounded-lg">
                <form action="{{ url('atualizar-produto/'.$product->id)}}" method="POST">
                    @csrf
                    @method('PUT')

                    <div>
                        <x-input-label for="name" :value="__('Nome do Produto')" />
                        <x-text-input id="name" class="block mt-1 w-full" type="text" name="name" :value="$product->name" required autofocus autocomplete="username" />
                    </div>
                    <div>
                        <x-input-label for="description" :value="__('Descrição do Produto')" />
                        <x-text-input id="description" class="block mt-1 w-full" type="text" name="description" :value="$product->description" required autofocus autocomplete="username" />
                    </div>
                    <div>
                        <x-input-label for="value" :value="__('Valor do Produto')" />
                        <x-text-input id="value" class="block mt-1 w-full" type="text" name="value" :value="$product->value" required autofocus autocomplete="username" />
                    </div>
                    <div>
                        <x-primary-button class="ml-3">{{ __('Atualizar Produto') }}</x-primary-button>
                    </div>

                </form>
            </div>
        </div>
    </div>

</x-app-layout>

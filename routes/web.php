<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
    Route::post('/logout', [ProfileController::class, 'logout'])->name('logout');
});

require __DIR__.'/auth.php';

Route::controller(App\Http\Controllers\ProductController::class)->group(function () {
    Route::get('/produtos', 'index');
    Route::get('/adicionar-produto', 'create');
    Route::post('/adicionar-produto', 'store');
    Route::get('/editar-produto/{product_id}', 'edit');
    Route::put('/atualizar-produto/{product_id}', 'update');
    Route::delete('/deletar-produto/{product_id}', 'destroy');
});


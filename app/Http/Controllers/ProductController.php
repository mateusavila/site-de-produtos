<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductFormRequest;
use App\Models\Product;
use Illuminate\Support\Facades\Auth;


use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $products = $user->products;
        return view('product.index', compact('products'));
    }

    public function create()
    {
        return view('product.create');
    }
 
    public function store(ProductFormRequest $request)
    {
    $data = $request->validated();
    $data['user_id'] = Auth::id();
    $product = Product::create($data);

    return redirect('/adicionar-produto')->with('message', 'Produto Adicionado Com Sucesso!');
    }


    public function edit($product_id)
    {
        $product = Product::find($product_id);

        return view('product.edit', compact('product'));
    }

    public function update(ProductFormRequest $request, $product_id)
    {
        $data = $request->validated();

        $product = Product::where('id', $product_id)->update([
            'name'=> $data['name'],
            'description'=> $data['description'],
            'value'=> $data['value'],
        ]);

        return redirect('/produtos')->with('message', 'Produto Atualizado Com Sucesso!');
    }

    public function destroy($product_id)
    {
        $product = Product::find($product_id)->delete();
        return redirect('/produtos')->with('message', 'Produto Deletado Com Sucesso!');
    }
}
